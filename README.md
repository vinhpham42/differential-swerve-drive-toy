# Differential Swerve DriveV2 Toy

Toy representation of a differential swerve drive. The design was orignally intended for a robot, however after losing intrest in the design, it was turned into a desk toy.

![Differentail-Swerve-Drive-Toy](https://gitlab.com/vinhpham42/differential-swerve-drive-toy/-/raw/master/Renders/ISO%20View.PNG)

# BOM
```
McMaster BOM
    4-40 Heat set Insert                QTY46       93365A120
    4-40 Button Head .1875in Length     QTY46       92949A105
    4-40 Socket Head 1.5in Length       QTY4        92196A119
    4-40 Nylock Nut                     QTY4        91831A005
    10-32 Button Head .5in Length       QTY2        92949A265
    #10 Washer Oversized                QTY2        90313A200
    
Bearing BOM
    VexPro 3/8 Thunderhex Flanged bearings      QTY2        217-5829
    60x75x7mm bearing (Any Vender)              QTY2        6712ZZ

Other BOM
    VexPro 3/8" Thunder Hex Stock(2.747" length) 	   QTY1        217-5837               
    VexPro Colson Performa (3" x 1.25" x 1/2" hex bore)    QTY1        217-4046
    
```
This lists the minimum amount to get started, be sure to adjust accordingly for spares.

# Tools
1. 1/16 Allen
2. 3/32 Allen
3. 1/8  Allen
4. 10-32 Tap
5. #4 Heat set installation tool 

# Assembly Instructions 
### Step 1 
Cut a piece of 3/8" Thunder Hex that is 2.747" long. Then tap each end with a 10-32 tap to a depth of at least .5". Slide on the 15T bevel gear, Gear Spacer, Thunderhex bearing, Colson wheel(with Hex Adapter installed), and another Thunderhex bearing. Keep in mind the direction of the flanges matches the picture below. If the Thunderhex bearing rubs on the wheel, use Shim Spacers between the wheel and bearing to reudce the friction. Lastly, use a 10-32 button head and washer to secure both sides of the shaft. 

![Differentail-Swerve-Drive-Toy](https://gitlab.com/vinhpham42/differential-swerve-drive-toy/-/raw/master/Renders/Step%201.PNG)
### Step 2
(Step 2 and 3 not required if KA040XP0 bearing not obtained)
Push on KA040XP0 bearing till flush with flanges. Using a heat set installation tool, push in 4-40 heat sets to be flush with the 3d printed part. 

![Differentail-Swerve-Drive-Toy](https://gitlab.com/vinhpham42/differential-swerve-drive-toy/-/raw/maseter/Renders/Step%202.PNG)
### Step 3
Using part "Large Bearing Inner Capture", secure the bearing in place with 4-40 button head bolts. 

![Differentail-Swerve-Drive-Toy](https://gitlab.com/vinhpham42/differential-swerve-drive-toy/-/raw/master/Renders/Step%203.PNG)
### Step 4
Push on 6712ZZ bearing till flush on "Bearing Support Top". Using a heat set installation tool, push in 4-40 heat sets to be flush with the 3d printed part. Repeat this step for "Bearing Support Bottom". Remove bearings for step 7. 

![Differentail-Swerve-Drive-Toy](https://gitlab.com/vinhpham42/differential-swerve-drive-toy/-/raw/master/Renders/Step%204.PNG)
### Step 5
Using parts "Bearing Support Bottom" and "Bearing Support Top" sandwitch the wheel assembly to the "Mid Plate". Insert 1.5in 4-40 socket head bolt from the bottom. 

![Differentail-Swerve-Drive-Toy](https://gitlab.com/vinhpham42/differential-swerve-drive-toy/-/raw/master/Renders/Step%205.PNG)
### Step 6
Secure assembly together using 4-40 nylock nuts captured by "Bearing Support Top"

![Differentail-Swerve-Drive-Toy](https://gitlab.com/vinhpham42/differential-swerve-drive-toy/-/raw/master/Renders/Step%206.PNG)
### Step 7
Install 6712ZZ bearing till almost flush to part "Combined 84T and Bevel". Using a heat set installation tool, push in 4-40 heat sets to be flush with the 3d printed part. Repeat step for second "Combined 84T and Bevel". 

![Differentail-Swerve-Drive-Toy](https://gitlab.com/vinhpham42/differential-swerve-drive-toy/-/raw/master/Renders/Step%207.PNG)
### Step 8
Mate gear and bearing assembly to "Bearing Support Top" and fasten down using part "Small Bearing Inner Capture" and 4-40 button heads. Repeat this step to mate gear and bearing assembly to "bearing Support Bottom"

![Differentail-Swerve-Drive-Toy](https://gitlab.com/vinhpham42/differential-swerve-drive-toy/-/raw/master/Renders/Step%208.PNG)
### Step 9
Secure 6712ZZ bearing using part "Small Bearing Outer Capture" and 4-40 button heads to "Bearing Support Top". Repeat this step for "Bearing Supprt Bottom". 

![Differentail-Swerve-Drive-Toy](https://gitlab.com/vinhpham42/differential-swerve-drive-toy/-/raw/master/Renders/Step%209.PNG) 
